import src.db_controller as db
import src.Ump.robo_ump as robo_ump

mlr = 'mlr'
milr = 'mlr'
season = 8

for i in range(12):
    session = i+1
    games = db.fetch_data('SELECT sheetID, awayTeam FROM gameData WHERE league=%s AND season=%s AND session=%s', (mlr, season, session))
    for game in games:
        sheet_id, team = game
        league, season, session, game_id = robo_ump.fetch_game_team(team, season, session)
        print(f'Auditing game log for {league.upper()} {season}.{session}.{game_id}...')
        robo_ump.audit_game_log(league, season, session, game_id, sheet_id)

    games = db.fetch_data('SELECT sheetID, awayTeam FROM gameData WHERE league=%s AND season=%s AND session=%s', (milr, season, session))
    for game in games:
        sheet_id, team = game
        league, season, session, game_id = robo_ump.fetch_game_team(team, season, session)
        print(f'Auditing game log for {league.upper()} {season}.{session}.{game_id}...')
        robo_ump.audit_game_log(league, season, session, game_id, sheet_id)
