import configparser
import os
from io import BytesIO
import datetime
import discord
import requests
from dhooks import Webhook
from discord.ext import commands
import src.db_controller as db
import src.Cogs.player as p
import src.Ump.robo_ump as robo_ump
import src.assets as assets
import src.sheets_reader as sheets
from discord.ui import Button, View, Select
from PIL import Image, ImageColor, ImageDraw, ImageFont, ImageOps

config = configparser.ConfigParser()
config.read("config.ini")
# ump_admin = int(config['Discord']['ump_admin_role'])
website_manager = int(config["Discord"]["website_manager"])
league_ops_role = int(config["Discord"]["league_ops_role"])
lom_role = int(config["Discord"]["lom_role"])
ump_warden = int(config["Discord"]["ump_warden_role"])
batman = int(config["Discord"]["batman"])
# error_log = Webhook(config['Channels']['error_log_webhook'])
league_config = "league.ini"
config_ini = "config.ini"
loading_emote = "☑️"
mustard_emote = "<:Mustard:447970922377248770>"


class Admin(commands.Cog):

    def __init__(self, bot):
        self.bot = bot
        config = configparser.ConfigParser()
        config.read("config.ini")

    @commands.command(
        brief="Sets the webhook URL for team servers",
        description="Adds a webhook URL into the database for results to get posted to different servers."
        " Accepts the abbreviated team name and the URL as arguments.",
    )
    @commands.has_any_role(website_manager, ump_warden, lom_role, league_ops_role)
    async def add_webhook(self, ctx, team, *, url):
        hook = Webhook(url)
        try:
            hook.send("Test")
        except Exception as e:
            await ctx.send("Invalid webhook provided.")
            return
        sql = """UPDATE teamData SET webhook_url = %s WHERE abb=%s"""
        db.update_database(sql, (url, team.upper()))
        db_team = db.fetch_data("""SELECT * FROM teamData WHERE abb = %s""", (team,))
        if db_team[0][4] == url:
            await ctx.send("Webhook URL set successfully.")
        else:
            await ctx.send("Something went wrong.")

    @commands.command(
        brief="Sets the RGB hexcode for teams",
        description="Sets the RGB hexcode into the database for embeds."
        " Accepts the abbreviated team name and the hexcode as arguments.",
    )
    @commands.has_any_role(website_manager, ump_warden, lom_role, league_ops_role)
    async def add_hexcode(self, ctx, team, *, hexcode):
        hex_letters = "0123456789ABCDEF"  # Set up the letters allowed
        hexcode = (
            hexcode[1:].upper() if hexcode[0] == "#" else hexcode.upper()
        )  # Turn string to ucase and remove pound

        if len(hexcode) != 6:  # Check if length is 6 letters
            await ctx.send("Invalid hex code.")
            return

        for h in hexcode:  # Check if all letters are 0-9 or A-F
            if h not in hex_letters:
                await ctx.send("Invalid hex code.")
                return

        sql = """UPDATE teamData SET color=%s WHERE abb=%s"""
        db.update_database(sql, (hexcode, team.upper()))
        db_team = db.fetch_data("""SELECT * FROM teamData WHERE abb=%s""", (team,))
        if db_team[0][2] == hexcode:
            await ctx.send("Hexcode set successfully.")
        else:
            await ctx.send("Something went wrong.")

    @commands.command(
        brief="Sets the logo for teams",
        description="Sets the logo into the database for embeds."
        " Accepts the abbreviated team name and the logo URL as arguments.",
    )
    @commands.has_any_role(website_manager, ump_warden, lom_role, league_ops_role)
    async def add_logo(self, ctx, team, *, url):
        if url[0:4] != "http":  # Check if URL begins with http
            await ctx.send("Invalid URL.")
            return

        sql = """UPDATE teamData SET logo_url=%s WHERE abb=%s"""
        db.update_database(sql, (url, team.upper()))
        db_team = db.fetch_data("""SELECT * FROM teamData WHERE abb=%s""", (team,))
        if db_team[0][3] == url:
            await ctx.send("Team logo URL set successfully.")
        else:
            await ctx.send("Something went wrong.")

    @commands.has_role(league_ops_role)
    @commands.command(
        brief="Generate draft images",
        description="Generate images for players being drafted.",
    )
    async def draft(self, ctx, draft_round, draft_pick, team, *, player_name):
        await ctx.message.add_reaction(loading_emote)
        team_data = db.fetch_data(
            "SELECT name, color, logo_url FROM teamData WHERE abb=%s", (team,)
        )
        if not team_data:
            await ctx.send("Could not find team %s." % team)
            return
        player = await p.get_player(ctx, player_name)
        if not player:
            await ctx.send("Wait who?")
            return
        
        confirm = Button(label="Send to Draft Channel", style=discord.ButtonStyle.green)
        cancel = Button(label="Cancel", style=discord.ButtonStyle.red)

        mlr_backend = read_config(config_ini, "URLs", "backend_sheet_id")

        async def send_request(interaction):
            draft_channel = self.bot.get_channel(
                int(config["Discord"]["draft_channel"])
            )
            draft_channel2 = self.bot.get_channel(
                int(config["Discord"]["draft_channel2"])
            )
            file = discord.File("draft1.jpg", filename="draft1.jpg")
            file2 = discord.File("draft2.jpg", filename="draft2.jpg")
            await draft_channel.send(file=file)
            await draft_channel2.send(file=file2)
            os.remove("draft1.jpg")
            os.remove("draft2.jpg")
            await interaction.response.defer()
            await prompt.edit(content="Image Sent.", view=None, embed=None)
            return

        async def cancel_request(interaction):
            await interaction.response.defer()
            await prompt.edit(content="Request cancelled.", view=None, embed=None)
            return

        sheet_id = sheets.get_sheet_id(config["URLs"]["fcb_roster"])
        player_stats = sheets.read_sheet(sheet_id, "Player Stats")
        fcb_stats1 = ""
        fcb_stats2 = ""
        for stats in player_stats:
            if stats[0] == player[1]:
                if player[7] == "P":
                    fcb_stats1 = "%s IP %s ERA" % (stats[44], stats[68])
                    fcb_stats2 = "%s WHIP %s DBF" % (stats[70], stats[77])
                else:
                    fcb_stats1 = "%s AVG %s OBP" % (stats[17], stats[18])
                    fcb_stats2 = "%s SLG %s DPA" % (stats[19], stats[38])

        image_size = (1920, 1080)
        font_name = "arial.ttf"

        pictureURL = r"./images/bgs/{}_bg.png".format(team.upper())

        # Background
        img = Image.open(pictureURL)
        img = img.resize(image_size, Image.LANCZOS)
        overlay = Image.new(
            mode="RGB",
            size=image_size,
            color=ImageColor.getrgb("#%s" % team_data[0][1]),
        )
        img = Image.blend(img, overlay, 0.2)
        overlay = Image.new(mode="RGB", size=image_size, color=(0, 0, 0))
        img = Image.blend(img, overlay, 0.7)
        img = ImageOps.expand(img, border=20, fill=(255, 255, 255))

        # Team Logo
        team_logo = r"./images/logos/{}.png".format(team.lower())
        logo = Image.open(team_logo)
        logo = logo.resize((200, 200), Image.LANCZOS)
        img.paste(logo, (75, image_size[1]-275), logo)
        # response = requests.get(team_data[0][2])
        # logo = Image.open(BytesIO(response.content))
        # logo = logo.resize((200,200), Image.LANCZOS)
        # img.paste(logo, (75, image_size[1] - 200 - 75), logo)

        draw_text = ImageDraw.Draw(img)
        team_font = ImageFont.truetype(font_name, 150)
        draft_font = ImageFont.truetype(font_name, 80)
        player_font = ImageFont.truetype(font_name, 225)
        position_font = ImageFont.truetype(font_name, 100)
        stats_font = ImageFont.truetype(font_name, 64)

        # Team Name
        text_size = draw_text.textlength(team_data[0][0], font=team_font)
        draw_text.text(
            (((image_size[0] - text_size) / 2) + 10, 75 + 10),
            team_data[0][0],
            font=team_font,
            fill=(0, 0, 0),
        )  # shadow
        draw_text.text(
            ((image_size[0] - text_size) / 2, 75),
            team_data[0][0],
            font=team_font,
            fill=ImageColor.getrgb("#%s" % team_data[0][1]),
            stroke_width=1,
            stroke_fill=(255, 255, 255),
        )

        # Draft Round/Pick
        draft_round_pick = "Round %s - Pick %s" % (draft_round, draft_pick)
        text_size = draw_text.textlength(draft_round_pick, font=draft_font)
        draw_text.text(
            (((image_size[0] - text_size) / 2) + 5, 285 + 5),
            draft_round_pick,
            font=draft_font,
            fill=(0, 0, 0),
        )
        draw_text.text(
            (((image_size[0] - text_size) / 2), 285),
            draft_round_pick,
            font=draft_font,
            fill=(255, 255, 255),
        )

        # Player Name
        name_region = (150, 400, 1810, 675)
        name_text = player[1]
        font_size = 500
        name_x = int((name_region[2] - name_region[0]) / 2) + name_region[0]
        name_y = int((name_region[3] - name_region[1]) / 2) + name_region[1]

        while font_size > 1:
            name_bbox = draw_text.textbbox((name_x, name_y), name_text, font=player_font, anchor="mm")
            if name_bbox[0] > name_region[0] and name_bbox[1] > name_region[1] and name_bbox[2] < name_region[2] and name_bbox[3] < name_region[3]:
                break
            font_size -= 1
            player_font = player_font.font_variant(size=font_size)

        draw_text.text(     # Draw name shadow
            (name_x+10, name_y+10),
            name_text,
            font=player_font,
            fill=(0, 0, 0),
            anchor="mm"
            )
        
        draw_text.text(     # Draw name in team color
            (name_x, name_y),
            name_text,
            font=player_font,
            fill=(255, 255, 255),
            stroke_width=5,
            stroke_fill=ImageColor.getrgb("#%s" % team_data[0][1]),
            anchor="mm"
            )

        # Positions
        positions = "%s %s" % (player[2], player[7])  # FCB Team, primary position
        if player[8]:
            positions += "/%s" % player[8]  # secondary position
        if player[9]:
            positions += "/%s" % player[9]  # tertiary position
        text_size = draw_text.textlength(positions, font=position_font)
        draw_text.text(
            (((image_size[0] - text_size) / 2) + 10, 700 + 10),
            positions,
            font=position_font,
            fill=(0, 0, 0),
        )
        draw_text.text(
            (((image_size[0] - text_size) / 2), 700),
            positions,
            font=position_font,
            fill=(255, 255, 255),
        )

        img.save("draft.jpg")
        img.save("draft1.jpg")
        img.save("draft2.jpg")
        file = discord.File("draft.jpg", filename="draft.jpg")

        # os.remove("draft.jpg")

        confirm.callback = send_request
        cancel.callback = cancel_request

        view = View(timeout=None)
        view.add_item(confirm)
        view.add_item(cancel)
        prompt = await ctx.send(file=file, view=view)

    @commands.has_any_role(website_manager, ump_warden, lom_role, batman)
    @commands.command()
    async def exit(self, ctx):
        await ctx.send("Bye")
        exit()

    @commands.has_any_role(website_manager, batman)
    @commands.command()
    async def fcb_config(self, ctx, fcb_var, var_value):
        var_list = ['fcb_ab_bat_pings_channel', 'fcb_roster', 'fcb_pa_log_backend_sheet_id', 'fcb_backend_sheet_id']
        if fcb_var in var_list:
            if fcb_var == 'fcb_ab_bat_pings_channel':
                section = 'Channels'
            else:
                section = 'URLs'
            write_config(config_ini, section, fcb_var, var_value)
            await ctx.send(f'[{section}][{fcb_var}] set to: {var_value}. **Reset bot with `.exit` to take effect.**')
        else:
            ctx.send(f'Variable {fcb_var} is not allowed to be changed.')
            return
    
    @commands.has_any_role(website_manager, batman, league_ops_role)
    @commands.command()
    async def steal_config(self, ctx, link):
        write_config(config_ini, 'URLs', 'steals', link)
        await ctx.send(f'[URLs][steals] set to: {link}. **Reset bot with `.exit` to take effect.**')

    @commands.has_any_role(website_manager, batman)
    @commands.command()
    async def fcb_view_config(self, ctx):
        send_txt = ''
        send_txt += f"[Channels][fcb_ab_bat_pings_channel]: {read_config(config_ini, 'Channels', 'fcb_ab_bat_pings_channel')}\n"
        send_txt += f"[URLs][fcb_roster]: {read_config(config_ini, 'URLs', 'fcb_roster')}\n"
        send_txt += f"[URLs][fcb_pa_log_backend_sheet_id]: {read_config(config_ini, 'URLs', 'fcb_pa_log_backend_sheet_id')}\n"
        send_txt += f"[URLs][fcb_backend_sheet_id]: {read_config(config_ini, 'URLs', 'fcb_backend_sheet_id')}\n"

        await ctx.send(send_txt)
    
    @commands.command(brief="Gets discord IDs for players")
    @commands.has_role(website_manager)
    async def get_discord_ids(self, ctx):
        await ctx.message.add_reaction(loading_emote)

        for guild in self.bot.guilds:
            for member in guild.members:
                user = db.fetch_data(
                    """SELECT discordName, discordID FROM playerData WHERE discordName=%s""",
                    (str(member),),
                )
                if user:
                    user = user[0]
                    if not user[1]:
                        db.update_database(
                            """UPDATE playerData SET discordID=%s WHERE discordName=%s""",
                            (member.id, str(member)),
                        )
                        user = db.fetch_data(
                            """SELECT discordName, discordID FROM playerData WHERE discordName=%s""",
                            (str(member),),
                        )
                        if user[0][1]:
                            await log_message(
                                self.bot,
                                "Added discord ID to database for <@%s>" % member.id,
                            )
                        else:
                            await log_message(
                                self.bot, "Failed to set user ID for <@%s>" % member.id
                            )
        await ctx.send("Done.")

    @commands.command(
        brief="Removes a team's existing webhook URL",
        description="Removes a webhook URL from the database.\n\nNote: Tech role is required to use this"
        " command.",
    )
    @commands.has_any_role(website_manager, ump_warden, lom_role, batman)
    async def remove_webhook(self, ctx, team):
        sql = """UPDATE teamData SET webhook_url = %s WHERE abb=%s"""
        db.update_database(sql, (None, team.upper()))
        (webhook_url,) = db.fetch_one(
            """SELECT webhook_url FROM teamData WHERE abb = %s""", (team,)
        )
        if webhook_url is None:
            await ctx.send("Webhook URL reset successfully.")
        else:
            await ctx.send("Something went wrong.")

    @commands.command(
        brief="Set season number", description="Set season number in the config."
    )
    @commands.has_any_role(website_manager, ump_warden, lom_role, batman)
    async def set_season(self, ctx, league, season):
        write_config(league_config, league.upper(), "season", season)
        sql = """UPDATE seasonData SET season=%s WHERE league=%s"""
        db.update_database(sql, (season, league))
        await ctx.send(
            "%s season set to %s."
            % (league, read_config(league_config, league.upper(), "season"))
        )

    @commands.command(
        brief="Set GotM thread", description="Set GotM thread in the config by team."
    )
    @commands.has_any_role(website_manager, ump_warden, lom_role, batman)
    async def set_gotm(self, ctx, team):
        season, session = robo_ump.get_current_session(team)
        league, season, session, game_id = robo_ump.fetch_game_team(
            team, season, session
        )
        sql = "SELECT threadURL FROM gameData WHERE league=%s AND season=%s AND session=%s AND gameID=%s"
        thread_url = db.fetch_one(sql, (league, season, session, game_id))
        if thread_url:
            write_config(
                league_config, "GotM", "game_of_the_moment_thread", thread_url[0]
            )

            gotm_channel = robo_ump.get_gotm_channel(self.bot)

            if ctx.channel != gotm_channel:
                await ctx.send(
                    "Thread set to <%s>."
                    % (read_config(league_config, "GotM", "game_of_the_moment_thread"))
                )
            game = robo_ump.fetch_game_team(team, season, session)
            sql = """SELECT sheetID, awayTeam, homeTeam, awayScore, homeScore, inning, outs, obc, complete, threadURL, winningPitcher, losingPitcher, save, potg, honorMention, state FROM gameData WHERE league=%s AND season=%s AND session=%s AND gameID=%s"""
            (
                sheet_id,
                awayTeam,
                homeTeam,
                awayScore,
                homeScore,
                inning,
                outs,
                obc,
                complete,
                threadURL,
                winningPitcher,
                losingPitcher,
                save,
                potg,
                hm,
                state,
            ) = db.fetch_one(sql, game)
            color, logo = db.fetch_one(
                """SELECT color, logo_url FROM teamData WHERE abb=%s""", (team,)
            )
            color = discord.Color(value=int(color, 16))
            title = f"Channel changed to {awayTeam} vs. {homeTeam}"
            embed = discord.Embed(title=title, color=color, url=threadURL)
            if state in ["SETUP", "WAITING FOR LINEUPS"]:
                description = f"{awayTeam: <4} {awayScore: <2}     ○     T1\n"
                description += f"{homeTeam: <4} {homeScore: <2}   ○   ○   {outs} Out"
                embed = discord.Embed(
                    title=title,
                    description=f"```{description}```",
                    color=color,
                    url=threadURL,
                )
                embed.add_field(name="Status", value=state.title())
            else:
                matchup_names = sheets.read_sheet(
                    sheet_id, assets.calc_cell2["current_matchup"]
                )
                matchup_info = sheets.read_sheet(
                    sheet_id, assets.calc_cell2["matchup_info"]
                )
                line_score = sheets.read_sheet(
                    sheet_id, assets.calc_cell2["line_score"]
                )

                line_1 = line_score[0][0]
                line_1 = line_1.replace("|", " ")
                line_1 = line_1.replace("*", "")
                line_2 = line_score[2][0]
                line_2 = line_2.replace("|", " ")
                line_2 = line_2.replace("*", "")
                line_3 = line_score[3][0]
                line_3 = line_3.replace("|", " ")
                line_3 = line_3.replace("*", "")
                line_score = f"   {line_1}\n{line_2}\n{line_3}"

                if matchup_names:
                    batter_name = matchup_names[0][0]
                    pitcher_name = matchup_names[0][2]
                else:
                    return None
                if matchup_info:
                    (
                        batter_id,
                        batter_type,
                        batter_hand,
                        pitcher_id,
                        pitcher_type,
                        pitcher_hand,
                        pitcher_bonus,
                    ) = matchup_info[0]
                else:
                    return None

                # Setting up OBC text
                b1 = "○"
                b2 = "○"
                b3 = "○"
                if obc in [1, 4, 5, 7]:
                    b1 = "●"
                if obc in [2, 4, 6, 7]:
                    b2 = "●"
                if obc in [3, 5, 6, 7]:
                    b3 = "●"

                # Shorten names to last name only if longer than 15 characters
                while len(batter_name) > 15:
                    batter_name = batter_name.split(" ", 1)
                    if len(batter_name) > 1:
                        batter_name = batter_name[1]
                    else:
                        batter_name = batter_name[0]
                        break

                while len(pitcher_name) > 15:
                    pitcher_name = pitcher_name.split(" ", 1)
                    if len(pitcher_name) > 1:
                        pitcher_name = pitcher_name[1]
                    else:
                        pitcher_name = pitcher_name[0]
                        break

                description = f"{line_score}\n\n"
                current_at_bat = (
                    f"{awayTeam:<4} {awayScore:>3}     {b2}        {inning}\n"
                )
                current_at_bat += (
                    f"{homeTeam:<4} {homeScore:>3}   {b3}   {b1}   {outs} Out\n\n"
                )
                current_at_bat += f"{pitcher_name: <15}  "
                current_at_bat += f"{pitcher_hand[0]}|{pitcher_type}-{pitcher_bonus}\n"
                current_at_bat += f"{batter_name: <15}  {batter_hand[0]}|{batter_type}"
                embed = discord.Embed(
                    title=title,
                    description=f"```{description}```",
                    color=color,
                    url=threadURL,
                )
                embed.add_field(
                    name="Current At Bat", value=f"```{current_at_bat}```", inline=False
                )
            embed.set_thumbnail(url=logo)
            await gotm_channel.send(embed=embed)

            new_embed = discord.Embed(
                title="Game Alert",
                description=f"This game is now being highlighted as the Game of the Moment! Head to {gotm_channel.mention} to support your team!",
            )
            await robo_ump.announce_to_team_webhook(awayTeam, new_embed)
            await robo_ump.announce_to_team_webhook(homeTeam, new_embed)

    @commands.command(
        brief="Set session number", description="Sets session number in the config."
    )
    @commands.has_any_role(website_manager, ump_warden, lom_role, batman)
    async def set_session(self, ctx, league, session):
        write_config(league_config, league.upper(), "session", session)
        sql = """UPDATE seasonData SET session=%s WHERE league=%s"""
        db.update_database(sql, (session, league))
        await ctx.send(
            "%s session set to %s."
            % (league, read_config(league_config, league.upper(), "session"))
        )
    
    @commands.command(
        brief="View servers",
        description="Views the servers the bot is located.",
        aliases=['view_servers']
    )
    @commands.has_any_role(website_manager, batman)
    async def servers_view(self, ctx):
        await ctx.send(f"The bot is in {len(self.bot.guilds)} servers.")
        
        message = ""
        i = 0
        for guild in self.bot.guilds:
            i += 1
            message += f"{guild.name} ({guild.id})\n"
            if i == 10:
                await ctx.send(message)
                message = ""
                i = 0
        await ctx.send(message)
    
    @commands.command(
        brief="Exit server",
        description="Exits the server by serverID given.",
        aliases=['exit_server', 'leave_server', 'server_leave']
    )
    @commands.has_any_role(website_manager, batman)
    async def servers_exit(self, ctx, sid):
        guild = discord.utils.get(self.bot.guilds, id=int(sid))
        if guild is None:
            message = "No guild with that id found."
        else:
            await guild.leave()
            message = f"Bot left {guild.name} ({guild.id})"
        await ctx.send(message)

    @commands.command(
        brief="Basebally", description="Shows a basebally", aliases=["basebally2"]
    )
    @commands.has_role(league_ops_role)
    async def basebally(self, ctx):
        await ctx.message.add_reaction(loading_emote)

    @commands.command(
        brief="Syncs the database to the backend sheet",
        description="Syncs the database with the backend sheet",
        aliases=["sync"],
    )
    @commands.has_role(league_ops_role)
    async def sync_database(self, ctx):
        await ctx.message.add_reaction(loading_emote)
        # Update player appointments...
        mlr_backend = read_config(config_ini, "URLs", "backend_sheet_id")
        milr_backend = read_config(config_ini, "URLs", "milr_backend_sheet_id")
        fcb_backend = read_config(config_ini, "URLs", "fcb_backend_sheet_id")
        mlr_roster = read_config(league_config, "Rosters", "mlr_s11")
        mlr_appointments = sheets.read_sheet(
            mlr_backend, assets.calc_cell["mlr_appointments"]
        )
        milr_appointments = sheets.read_sheet(
            milr_backend, assets.calc_cell["mlr_appointments"]
        )

        for team in mlr_appointments:
            cogm = ""
            captain1 = ""
            captain2 = ""
            captain3 = ""
            committee1 = ""
            committee2 = ""
            awards1 = ""
            awards2 = ""
            abb = team[0]
            gm = team[1]
            if len(team) >= 3:
                cogm_check = sheets.read_sheet(mlr_roster, "%s!I2" % abb)
                print(f"TEST: CoGM: {abb} : {cogm_check}")
                if cogm_check[0][0] == "Co-GM:":
                    cogm = team[2]
                else:
                    captain1 = team[2]
            if len(team) >= 4:
                captain2 = team[3]
            if len(team) >= 5:
                captain3 = team[4]
            if len(team) >= 6:
                committee1 = team[5]
            if len(team) >= 7:
                committee2 = team[6]
            if len(team) >= 8:
                awards1 = team[7]
            if len(team) >= 9:
                awards2 = team[8]
            team_data = (
                gm,
                cogm,
                captain1,
                captain2,
                captain3,
                committee1,
                committee2,
                awards1,
                awards2,
                abb,
            )
            sql = """UPDATE teamData SET gm=%s, cogm=%s, captain1=%s, captain2=%s, captain3=%s, committee1=%s, committee2=%s, awards1=%s, awards2=%s WHERE abb=%s"""
            db.update_database(sql, team_data)

        # Update MiLR Appointments
        for team in milr_appointments:
            gm = ""
            cogm = ""
            captain1 = ""
            captain2 = ""
            abb = team[0]
            if len(team) >= 2:
                gm = team[1]
            if len(team) >= 3:
                cogm = team[2]
            if len(team) >= 4:
                captain1 = team[3]
            if len(team) >= 5:
                captain2 = team[4]
            team_data = (gm, cogm, captain1, captain2, abb)
            sql = """UPDATE teamData SET gm=%s, cogm=%s, captain1=%s, captain2=%s WHERE abb=%s"""
            db.update_database(sql, team_data)

        # Update Discord Username In the Backend Sheet based on the Discord ID
        sheet_id = mlr_backend
        rows = sheets.read_sheet(sheet_id, "Player List Input")
        for i in range(len(rows)):
            row = rows[i]
            if row:
                if row[0] != "Player ID":
                    discord_id = row[12]
                    if discord_id:
                        user = ctx.bot.get_user(int(discord_id))
                        if user:
                            discord_name = user.name
                            if user.discriminator != "0":
                                discord_name += f"#{user.discriminator}"
                            if discord_name != row[11]:
                                sheets.update_sheet(
                                    sheet_id,
                                    "Player List Input!L%s" % (i + 1),
                                    discord_name,
                                )
                                await log_message(
                                    self.bot,
                                    "Updated discord name in Player List Input for `%s` to `%s`"
                                    % (row, discord_name),
                                )

        await ctx.message.add_reaction(mustard_emote)

        # Sync playerData with backend player list
        rows = sheets.read_sheet(sheet_id, "Player List Input")
        for row in rows:
            if row[0] != "Player ID" and len(row) > 0:
                player_id = int(row[0])
                player_name = row[1] if len(row) >= 2 else ''
                team = row[2] if len(row) >= 3 else ''
                batting_type = row[3] if len(row) >= 4 else ''
                pitching_type = row[4] if len(row) >= 5 else ''
                pitching_bonus = row[5] if len(row) >= 6 else ''
                hand = row[6] if len(row) >= 7 else ''
                pos1 = row[7] if len(row) >= 8 else ''
                pos2 = row[8] if len(row) >= 9 else ''
                pos3 = row[9] if len(row) >= 10 else ''
                reddit_name = row[10] if len(row) >= 11 else ''
                discord_name = row[11] if len(row) >= 12 else ''
                status = int(row[13]) if len(row) >= 14 else 0
                pos_value = int(row[14]) if len(row) >= 15 else 0
                milr_team = row[16] if len(row) >= 17 else ''
                player_in_sheet = (
                    player_name,
                    team,
                    batting_type,
                    pitching_type,
                    pitching_bonus,
                    hand,
                    pos1,
                    pos2,
                    pos3,
                    reddit_name,
                    discord_name,
                    status,
                    pos_value,
                    milr_team,
                    player_id,
                )
                sql = """SELECT playerName, Team, batType, pitchType, pitchBonus, hand, priPos, secPos, tertPos, redditName, discordName, Status, posValue, milr_team, playerID FROM playerData WHERE playerID = %s"""
                player_in_db = db.fetch_data(sql, (player_id,))
                if player_in_db:
                    player_in_db = player_in_db[0]
                else:
                    sql = """INSERT INTO playerData (playerName, Team, batType, pitchType, pitchBonus, hand, priPos, secPos, tertPos, redditName, discordName, Status, posValue, milr_team, playerID) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"""
                    db.update_database(sql, player_in_sheet)
                    await log_message(
                        self.bot, f"Added new player: `{player_in_sheet}`"
                    )
                    continue
                if player_in_db != player_in_sheet:
                    sql = """UPDATE playerData SET playerName=%s, Team=%s, batType=%s, pitchType=%s, pitchBonus=%s, hand=%s, priPos=%s, secPos=%s, tertPos=%s, redditName=%s, discordName=%s, Status=%s, posValue=%s, milr_team=%s WHERE playerID=%s"""
                    db.update_database(sql, player_in_sheet)
                    await log_message(
                        self.bot,
                        "Updated existing player from `%s` to `%s`"
                        % (player_in_db, player_in_sheet),
                    )

        sheet_id = fcb_backend
        # Add/Sync all players from college
        # rows = sheets.read_sheet(sheet_id, 'Player List Input')
        # for row in rows:
        #     if row[0] != 'Player ID':
        #         player_id = int(row[0])
        #         player_name = row[1]
        #         team = row[2]
        #         batting_type = row[3]
        #         pitching_type = row[4]
        #         pitching_bonus = row[5]
        #         hand = row[6]
        #         pos1 = row[7]
        #         pos2 = row[8]
        #         pos3 = row[9]
        #         reddit_name = row[10]
        #         discord_name = row[11]
        #         status = int(row[13])
        #         pos_value = int(row[14])
        #         player_in_sheet = (player_name, team, batting_type, pitching_type, pitching_bonus, hand, pos1, pos2, pos3, reddit_name, discord_name, status, pos_value, player_id)
        #         sql = '''SELECT playerName, Team, batType, pitchType, pitchBonus, hand, priPos, secPos, tertPos, redditName, discordName, Status, posValue, playerID FROM playerData WHERE playerID = %s'''
        #         player_in_db = db.fetch_data(sql, (player_id,))
        #         if player_in_db:
        #             player_in_db = player_in_db[0]
        #         else:
        #             sql = '''INSERT INTO playerData (playerName, Team, batType, pitchType, pitchBonus, hand, priPos, secPos, tertPos, redditName, discordName, Status, posValue, playerID) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)'''
        #             db.update_database(sql, player_in_sheet)
        #             await log_message(self.bot, f'Added new player: `{player_in_sheet}`')
        #             continue
        #         if player_in_db != player_in_sheet:
        #             sql = '''UPDATE playerData SET playerName=%s, Team=%s, batType=%s, pitchType=%s, pitchBonus=%s, hand=%s, priPos=%s, secPos=%s, tertPos=%s, redditName=%s, discordName=%s, Status=%s, posValue=%s WHERE playerID=%s'''
        #             db.update_database(sql, player_in_sheet)
        #             await log_message(self.bot, 'Updated existing player from `%s` to `%s`' % (player_in_db, player_in_sheet))
        await ctx.message.remove_reaction(loading_emote, ctx.bot.user)
        await ctx.send("Done.")


async def setup(bot):
    await bot.add_cog(Admin(bot))


def read_config(filename, section, setting):
    ini_file = configparser.ConfigParser()
    ini_file.read(filename)
    return ini_file[section][setting]


def write_config(filename, section, setting, value):
    ini_file = configparser.ConfigParser()
    ini_file.read(filename)
    ini_file.set(section, setting, value)
    with open(filename, "w") as configfile:
        ini_file.write(configfile)


async def log_message(bot, message: str):
    log_channel = bot.get_channel(int(config["Discord"]["log_channel"]))
    await log_channel.send(
        f"<t:{int(datetime.datetime.now().timestamp())}:T> - {message}"
    )
